# scs.c

scsc is a simple program to automatically move the mouse cursor to the center of one of the screens.

## Usage

```bash
./scsc [screen number]
```

## Building

### Dependencies
scsc needs the following packages installed:

- cmake
- gdk-3.0

### Build

To build, use the following script:

```bash
mkdir build
cd build
cmake ..
make
```
