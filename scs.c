#include <stdio.h>
#include <stdlib.h>
#include <gdk/gdk.h>

int main(int argc, char** argv) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <screen number>\n", argv[0]);
        return 1;
    }
    gdk_init(&argc, &argv);
    int screen = atoi(argv[1]);
    GdkDisplay *d = gdk_display_get_default();
    int num_monitors = gdk_display_get_n_monitors(d);
    if (screen >= num_monitors || screen < 0) {
        fprintf(stderr, "Invalid screen number %d, found %d monitors (0-%d)\n", screen, num_monitors, num_monitors-1);
        return 1;
    }
    GdkMonitor *m = gdk_display_get_monitor(d, screen);
    GdkRectangle workarea;
    gdk_monitor_get_workarea(m, &workarea);
    int center_x = workarea.x + workarea.width/2;
    int center_y = workarea.y + workarea.height/2;
    GdkSeat* seat = gdk_display_get_default_seat(d);
    GdkDevice* pointer = gdk_seat_get_pointer(seat);
    GdkScreen* gdk_screen = gdk_display_get_default_screen(d);
    gdk_device_warp(pointer, gdk_screen, center_x, center_y);
    gdk_display_flush(d);
}
